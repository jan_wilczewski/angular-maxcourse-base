import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { ServerComponent } from './MainProject/server/server.component';
import { ServersComponent } from './MainProject/servers/servers.component';
import { DatabidingEx1Component } from './Basics/databiding-ex1/databiding-ex1.component';
import { DirectivesEx2Component } from './Basics/directives-ex2/directives-ex2.component';
import { HeaderComponent } from './project1/header/header.component';
import { RecipesComponent } from './project1/recipes/recipes.component';
import { RecipeListComponent } from './project1/recipes/recipe-list/recipe-list.component';
import { RecipeDetailComponent } from './project1/recipes/recipe-detail/recipe-detail.component';
import { RecipeItemComponent } from './project1/recipes/recipe-list/recipe-item/recipe-item.component';
import { ShoppingListComponent } from './project1/shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './project1/shopping-list/shopping-edit/shopping-edit.component';
import { CockpitComponent } from './MainProject/cockpit/cockpit.component';
import { ServerElementComponent } from './MainProject/server-element/server-element.component';
import { GameControlComponent } from './project2/game-control/game-control.component';
import { OddComponent } from './project2/odd/odd.component';
import { EvenComponent } from './project2/even/even.component';
import { DropdownDirective } from './project1/shared/dropdown.directive';
import { AccountComponent } from './section9services/account/account.component';
import { NewAccountComponent } from './section9services/new-account/new-account.component';
import {AccountsService} from './section9services/logging/accounts.service';
import {LoggingService} from './section9services/logging/logging.service';
import {ShoppingListService} from './project1/shopping-list/shopping-list.service';


@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServersComponent,
    DatabidingEx1Component,
    DirectivesEx2Component,
    HeaderComponent,
    RecipesComponent,
    RecipeListComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    ShoppingListComponent,
    ShoppingEditComponent,
    CockpitComponent,
    ServerElementComponent,
    GameControlComponent,
    OddComponent,
    EvenComponent,
    DropdownDirective,
    AccountComponent,
    NewAccountComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [AccountsService, LoggingService, ShoppingListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
