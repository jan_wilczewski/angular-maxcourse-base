import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directives-ex2',
  templateUrl: './directives-ex2.component.html',
  styleUrls: ['./directives-ex2.component.css']
})
export class DirectivesEx2Component {

  pass = 'ssij';
  passwordVisible = false;
  passwords = [];

  constructor() { }

  onShowPassword() {
    this.passwordVisible = !this.passwordVisible;
    // this.passwords.push(this.passwords.length + 1);
    this.passwords.push(new Date());
  }


}
