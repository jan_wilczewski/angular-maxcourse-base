import {Component, OnInit} from '@angular/core';
import {AccountsService} from './section9services/logging/accounts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  //Project 1 - recipe book

  loadedFeature = 'recipe';

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }

  // //Section9
  // accounts: {name: string, status: string}[] = [];
  //
  // constructor(private accountService: AccountsService){}
  //
  // ngOnInit() {
  //   this.accounts = this.accountService.accounts;
  // }


  // section7
  // numbers = [1, 2, 3, 4, 5];
  // onlyOdd = false;

  // Section 5
  //
  // serverElements = [{type: 'server', name: 'Testserver', content: 'Just a test!'}];
  //
  // onServerAdded(serverData: {serverName: string, serverContent: string}) {
  //   this.serverElements.push({
  //     type: 'server',
  //     name: serverData.serverName,
  //     content: serverData.serverContent
  //   });
  // }
  //
  // onBlueprintAdded(blueprintData: {serverName: string, serverContent: string}) {
  //   this.serverElements.push({
  //     type: 'blueprint',
  //     name: blueprintData.serverName,
  //     content: blueprintData.serverContent
  //   });
  // }
  //
  // onChangeFirst() {
  //   this.serverElements[0].name = 'Changed!';
  // }
  //
  // onDestroyFirst() {
  //   this.serverElements.splice(0, 1);
  // }



  //
  // // Project2 - odd/even game
  //
  // oddNumbers: number[] = [];
  // evenNumbers: number[] = [];
  //
  // onIntervalFired(firedNumber: number) {
  //   if (firedNumber % 2 === 0){
  //     this.evenNumbers.push(firedNumber);
  //   }else {
  //     this.oddNumbers.push(firedNumber);
  //   }
  // }
}
